//
//  main.m
//  Recognizer
//
//  Created by Jay Roberts on 1/25/12.
//  Copyright (c) 2012 GloryFish.org. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, char *argv[])
{
    return NSApplicationMain(argc, (const char **)argv);
}
