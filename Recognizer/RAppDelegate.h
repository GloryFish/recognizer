//
//  RAppDelegate.h
//  Recognizer
//
//  Created by Jay Roberts on 1/25/12.
//  Copyright (c) 2012 GloryFish.org. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface RAppDelegate : NSObject <NSApplicationDelegate> {
}




@property (nonatomic, strong) IBOutlet NSMenu* statusMenu;
@property (nonatomic, strong) NSStatusItem* statusItem;
@property (nonatomic, strong) NSImage* statusImage;
@property (nonatomic, strong) NSImage* statusImageHighlight;

-(IBAction)quit:(id)sender;
-(IBAction)test:(id)sender;
@end
