//
//  RAppDelegate.m
//  Recognizer
//
//  Created by Jay Roberts on 1/25/12.
//  Copyright (c) 2012 GloryFish.org. All rights reserved.
//

#import "RAppDelegate.h"

@implementation RAppDelegate

@synthesize statusMenu = _statusMenu;
@synthesize statusItem = _statusItem;
@synthesize statusImage = _statusImage;
@synthesize statusImageHighlight = _statusImageHighlight;

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    // Insert code here to initialize your application
    self.statusItem = [[NSStatusBar systemStatusBar] statusItemWithLength:NSSquareStatusItemLength];
        
    self.statusImage = [[NSImage alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"statusicon" ofType:@"png"]];

    self.statusImageHighlight = [[NSImage alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"statusicon-highlight" ofType:@"png"]];

    [self.statusItem setTitle:@""];
    [self.statusItem setToolTip:@"My Tooltip"];
    [self.statusItem setImage:self.statusImage];
    [self.statusItem setAlternateImage:self.statusImageHighlight];
    
    [self.statusItem setMenu:self.statusMenu];
    
    [self.statusItem setHighlightMode:YES];
}

-(IBAction)test:(id)sender {
    NSLog(@"test");
}


-(IBAction)quit:(id)sender {
    NSLog(@"quit");
    [[NSApplication sharedApplication] terminate:sender];
}

@end
